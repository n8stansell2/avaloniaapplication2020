﻿namespace AvaloniaApplication2020.ViewModels
{
    using AvaloniaApplication2020.Database;
    using ReactiveUI;
    using Services;
    using System;
    using System.Reactive.Linq;
    using System.Runtime.InteropServices.ComTypes;

    public class MainWindowViewModel : ViewModelBase
    {
        private ViewModelBase _content;
        public ViewModelBase Content
        {
            get => _content;
            private set => this.RaiseAndSetIfChanged(ref _content, value);
        }

        private DatabaseHelper DatabaseHelper;
        public TodoListViewModel TodoList { get; private set; }
        
        public MainWindowViewModel()
        {
            
        }

        public void Init(DatabaseHelper dbHelper)
        {
            DatabaseHelper = dbHelper;
            TodoList = new TodoListViewModel(DatabaseHelper.GetItems());
            this.Content = TodoList;
        }

        public void AddItem()
        {
            AddTodoItemViewModel vm = new AddTodoItemViewModel();

            Observable.Merge(vm.OKCommand, vm.CancelCommand.Select(c => (Todo)null)).Take(1).Subscribe(x =>
            {
                if (x != null)
                {
                    DatabaseHelper.AddItem(x.Description);
                }

                TodoList.RefreshList(DatabaseHelper.GetItems());
                this.Content = TodoList;
            });

            this.Content = vm;
        }
    }
}
