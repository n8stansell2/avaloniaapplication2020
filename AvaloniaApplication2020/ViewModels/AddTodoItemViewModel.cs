﻿namespace AvaloniaApplication2020.ViewModels
{
    using Database;
    using ReactiveUI;
    using System;
    using System.Reactive;

    public class AddTodoItemViewModel : ViewModelBase
    {
        private string _description;
        public string Description
        {
            get => _description;
            set => this.RaiseAndSetIfChanged(ref _description, value);
        }

        public ReactiveCommand<Unit, Todo> OKCommand { get; private set; }
        public ReactiveCommand<Unit, Unit> CancelCommand { get; private set; }

        public AddTodoItemViewModel()
        {
            IObservable<bool> OkEnabled = this.WhenAnyValue(x => x.Description, x => !string.IsNullOrWhiteSpace(x));
            OKCommand = ReactiveCommand.Create(() => new Todo() { Description = this.Description, IsChecked = false }, OkEnabled);
            CancelCommand = ReactiveCommand.Create(() => { });
        }
    }
}
