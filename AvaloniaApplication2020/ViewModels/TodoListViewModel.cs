﻿namespace AvaloniaApplication2020.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Database;

    public class TodoListViewModel : ViewModelBase
    {
        public ObservableCollection<Todo> Items { get; private set; }
        
        public TodoListViewModel(IEnumerable<Todo> items)
        {
            Items = new ObservableCollection<Todo>(items);
        }

        public void RefreshList(IEnumerable<Todo> items)
        {
            this.Items.Clear();
            this.Items = new ObservableCollection<Todo>(items);
        }
    }
}
