﻿namespace AvaloniaApplication2020.Services
{
    using Database;
    using System.Collections.Generic;
    using System.Linq;

    public class DatabaseHelper
    {
        private EFDBContext db;
        
        public DatabaseHelper()
        {
            db = new EFDBContext();
        }
        
        public IEnumerable<Todo> GetItems()
        {
            List<Todo> result = (from x in db.Todos select x).ToList();

            return result;
        }

        public void AddItem(string description)
        {
            Todo rec = new Todo() { Description = description, IsChecked = false };
            db.Todos.Add(rec);
            db.SaveChanges();
        }
    }
}
