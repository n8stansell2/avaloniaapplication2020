﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AvaloniaApplication2020.Database
{
    public class Todo
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public bool IsChecked { get; set; }
    }
}
