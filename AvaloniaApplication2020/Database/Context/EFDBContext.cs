﻿namespace AvaloniaApplication2020.Database
{
    using Microsoft.EntityFrameworkCore;
    using System.ComponentModel.DataAnnotations.Schema;
    
    public class EFDBContext : DbContext
    {
        public DbSet<Todo> Todos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source=Database/DemoDB.sqlite");
        }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.Entity<Todo>().ToTable("Todos");
            mb.Entity<Todo>(e =>
            {
                e.HasKey(x => x.ID);
                e.Property(x => x.ID).IsRequired();
            });
        }
    }
}
