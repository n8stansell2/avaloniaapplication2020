﻿using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using AvaloniaApplication2020.ViewModels;
using AvaloniaApplication2020.Views;
using AvaloniaApplication2020.Services;

namespace AvaloniaApplication2020
{
    public class App : Application
    {
        private DatabaseHelper databaseHelper;
        private MainWindowViewModel mainWindowViewModel;
        
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            databaseHelper = new DatabaseHelper();
            
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                mainWindowViewModel = new MainWindowViewModel();
                mainWindowViewModel.Init(this.databaseHelper);
                
                desktop.MainWindow = new MainWindow
                {
                    DataContext = mainWindowViewModel,
                };
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}
